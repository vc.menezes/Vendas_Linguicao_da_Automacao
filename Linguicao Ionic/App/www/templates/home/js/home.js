var homeControllers = angular.module('home.controllers', []);

/*--------------------------------Lógica para a tela de atrações da festa---------------------------------*/

homeControllers.controller('HomeController',
    function ($scope,
              DataLoader,
              $ionicHistory,
              $ionicPlatform,
              $cordovaSplashscreen,
              $ionicModal) {

        $ionicHistory.clearHistory()

        DataLoader.load('data/main/anuncios.json').then(function (response) {
                $scope.anuncios = response;
            }
        )

        $ionicModal.fromTemplateUrl('templates/home/html/gandhi.html', {
            animation: 'slide-in-up',
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });

        $scope.abreGandhi = function () {
            $scope.modal.show()
        }
        $scope.fechaGandhi = function () {
            $scope.modal.hide()
        }

        $ionicPlatform.ready(function () {
            //$cordovaSplashscreen.hide();
        });
    });
