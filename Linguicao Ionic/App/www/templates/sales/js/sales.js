var salesControllers = angular.module('sales.controllers', ['sales.services', 'helpers.dataloader', 'ionic', 'ngCordova'])
var vendedores_display = [];
var vendedores = [];
/*--------------------------------Lógica para a tela de vendas de ingresso--------------------------------*/
salesControllers.controller('SalesEventosController', function ($scope, DataLoader) {
    DataLoader.load('data/sales/eventos.json').then(function (response) {
            $scope.eventos = response;
        }
    );
});

salesControllers.controller('SalesPontosController', function ($scope, DataLoader) {
    DataLoader.load('data/sales/pontos.json').then(function (response) {
            $scope.pontos = response;
        }
    );
});

salesControllers.controller('VendedoresController', function ($scope, $ionicPopup, $cordovaToast, VendedoresService, $rootScope, $ionicPlatform, $ionicScrollDelegate, $cordovaGeolocation) {
    var posOptions = {timeout: 15000, enableHighAccuracy: true};
    $scope.GetVendedores = function () {
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            myLat = position.coords.latitude;
            myLng = position.coords.longitude;
            console.log(myLat, myLng);

            VendedoresService.findAll().then(function (v) {
                vendedores = v;
                console.log(vendedores);
                $scope.vendedores = vendedores;
                VendedoresService.getUniversidades().then(function (universidades) {
                    $scope.universidades = universidades;
                });
                $rootScope.hide();
            }, function (err) {
                $cordovaToast.show("Sem conexão!", "short", "center");
                GetVendedoresOffline();
            })
        }, function (err) {
            $cordovaToast.show("Sem localização!", "short", "center");
            //$rootScope.hide();
            //var confirmPopup = $ionicPopup.confirm({
            //    title: 'GPS',
            //    template: 'Houve um erro ao pegar sua posição, ' +
            //    'deseja tentar novamente?'
            //});
            //confirmPopup.then(function (res) {
            //    if (res) {
            //        $rootScope.show();
            //        $timeout(
            //            function () {
            //                $scope.GetVendedores();
            //            },
            //            10000
            //        );
            //    }
            //    else {
            //        $cordovaToast.show("Dados sem localização!", "short", "center");
            //        GetVendedoresOffline();
            //    }
            //})
        });
    };

    function GetVendedoresOffline() {
        VendedoresService.getVendedoresOff().then(function (v) {
            vendedores = v;
            console.log(vendedores);
            $scope.vendedores = vendedores;
            VendedoresService.getUniversidades().then(function (universidades) {
                $scope.universidades = universidades;
            });
            //$cordovaToast.show("Success", 'long', 'center');
            $rootScope.hide();
        }).catch(function (err) {
            //$cordovaToast.show("Erro GetVendedoresOff " + err, 'long', 'center');
            $rootScope.hide();
        })
    };

    $scope.$watch('search.universidade', function (s) {
        $ionicScrollDelegate.scrollTop(false);
        vendedores_display = [];
        console.log(s);
        if (s == "") {
            console.log(vendedores);
            vendedores_display = vendedores;
        }
        else {
            console.log(vendedores);
            angular.forEach(vendedores, function (obj) {
                if (obj.universidade == s) {
                    vendedores_display.push(obj);
                }
            });
        }
        console.log(vendedores_display);
        $scope.vendedores = vendedores_display;
        $ionicScrollDelegate.scrollTop(true);
    });

    document.addEventListener("offline", onOffline, false);
    document.addEventListener("online", onOnline, false);

    function onOffline() {
        GetVendedoresOffline();
    }

    function onOnline() {
        $scope.GetVendedores();
    }

    var netSuccess = function () {
        $rootScope.show();
        cordova.exec(gpsSuccess, gpsFail, "LocationAndSettings", "isGpsEnabled", []);
    };

    var netFail = function () {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Internet',
            template: 'Você não está conectado à internet, ' +
            'deseja ir até as configurações para ativá-la?'
        });
        confirmPopup.then(function (res) {
            if (res) {
                cordova.exec(wifiSuccess, wifiFail, "LocationAndSettings", "switchToWifiSettings", []);
            }
            else {
                $cordovaToast.show("Não conectado à internet!", "short", "center");
                GetVendedoresOffline();
            }
        })
    };

    var wifiSuccess = function () {
        cordova.exec(gpsSuccess, gpsFail, "LocationAndSettings", "isGpsEnabled", []);
    };

    var wifiFail = function () {
        $cordovaToast.show("Não conectado à internet!", "short", "center");
    };

    var gpsSuccess = function (data) {
        if (data == true) {
            $rootScope.show();
            $scope.GetVendedores();
        }
        else {
            $rootScope.hide();
            var confirmPopup = $ionicPopup.confirm({
                title: 'GPS',
                template: 'Você não está com o GPS ativado, ' +
                'deseja ir até as configurações para ativá-lo?'
            });
            confirmPopup.then(function (res) {
                if (res) {
                    cordova.exec(settingsSuccess, settingsFail, "LocationAndSettings", "switchToLocationSettings", []);
                }
                else {
                    $cordovaToast.show("Serviço de localização desativado!", "short", "center");
                    GetVendedoresOffline();
                }
            })
        }
    };
    var gpsFail = function (data) {
        $cordovaToast.show("Serviço de localização desativado!", "short", "center");
    };

    var settingsSuccess = function () {
        $rootScope.show();
        $timeout(
            function () {
                $scope.GetVendedores();
            },
            7000
        );
    };

    var settingsFail = function () {
        $cordovaToast.show("Serviço de localização desativado!", "short", "center");
        GetVendedoresOffline();
    };

    $ionicPlatform.ready(function () {
        if (angular.equals([], vendedores)) {
            if (window.Connection) {
                if (navigator.connection.type != Connection.NONE) {
                    netSuccess();
                }
                else {
                    netFail();
                }
            }
        }
    })
});

salesControllers.controller('VendedorCtrl', function ($scope, $cordovaToast, $ionicPopup, $stateParams, VendedoresService, $cordovaSms, $ionicPlatform) {
    var tel;
    $scope.getVendedor = function () {
        $scope.vendedor = VendedoresService.findByName($stateParams.vendedorId);
        tel = $scope.vendedor.ddd + $scope.vendedor.celular;
    }

    $scope.SMS = function () {
        var options = {
            replaceLineBreaks: false, // true to replace \n by a new line, false by default
            android: {
                intent: 'INTENT'  // send SMS with the native android SMS messaging
            }
        };
        $cordovaSms.send(tel, 'Oi, ' + $scope.vendedor.nome, options).then(function () {
        }, function (error) {
            console.log(error);
        });
    };

    $scope.OpenWhats = function () {

        VendedoresService.getContato(tel).then(function (contato) {
            $cordovaToast.show(contato, 'short', 'center');
            if (contato != null) {
                if (window.Connection) {
                    if (navigator.connection.type != Connection.NONE) {
                        openWhats();
                    }
                    else {
                        openWhats();
                    }
                }
            }
            else {
                $cordovaToast.show(contato, 'short', 'center');
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Contato',
                    template: 'Você não possui este contato, deseja adicioná-lo?'
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        VendedoresService.saveContato(tel, $scope.vendedor.nome).then(function (contato) {
                            if (window.Connection) {
                                if (navigator.connection.type != Connection.NONE) {
                                    $cordovaToast.show("Atualize seus contatos!", 'long', 'center');
                                    openWhats();
                                }
                                else {
                                    $cordovaToast.show("Você deve se conectar à internet e atualizar seus contatos!", 'long', 'center');
                                }
                            }
                        })
                    }
                    else {
                    }
                });
            }
        }, function (error) {
            $cordovaToast.show("Erro", 'short', 'center');
        })
    }

    $scope.CallTel = function (tel) {
        console.log(tel);
        window.location.href = 'tel:' + tel;
    }

    $scope.OpenFacebook = function () {
        if ($scope.vendedor.status == null) {
            window.open($scope.vendedor.fb_link, '_system', 'location=yes');
        }
        else {
            window.open($scope.vendedor.facebook, '_system', 'location=yes');
        }
    }

    function openWhats() {
        navigator.startApp.start("com.whatsapp", function (message) {  /* success */
                $cordovaToast.show("SUCESSO", 'long', 'center');
            },
            function (error) { /* error */
                $cordovaToast.show("ERRO", 'long', 'center');
            });
    }

    $ionicPlatform.ready(function () {
        $scope.getVendedor();
    });
});

