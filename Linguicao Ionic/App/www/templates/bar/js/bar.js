var barControllers = angular.module('bar.controllers', []);

/*--------------------------------Lógica para a tela de preços do bar---------------------------------*/

barControllers.controller('BarController', function($scope, BarService) {

    var getDrinkPrices = function() {
        BarService.getDrinkPrices().then(function (drinks) {
            $scope.drinks = drinks;
        });
    };

    var getComboPrices = function() {
        BarService.getComboPrices().then(function (combos) {
            $scope.combos = combos;
        });
    };

    getDrinkPrices();
    getComboPrices();
});
