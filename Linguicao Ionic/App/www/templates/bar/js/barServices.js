var barService = angular.module('bar.services', []);

barService.factory('BarService', function($q, $http) {

    function Bar(name, price) {
        this.name = name;
        this.price = price;
    }

    var drinkPrices = function() {

        var defer = $q.defer();

        $http.get('data/bebidas.json')
            .success(function(data) {
                defer.resolve(data);
            })
            .error(function() {
                console.log('could not find file');
                defer.reject('could not find someFile.json');
            });

        return defer.promise;
    };

    var comboPrices = function() {

        var defer = $q.defer();

        $http.get('data/combos.json')
            .success(function(data) {
                defer.resolve(data);
            })
            .error(function() {
                console.log('could not find file');
                defer.reject('could not find someFile.json');
            });

        return defer.promise;
    };

    return {
        getDrinkPrices: function() {
            return drinkPrices();
        },
        getComboPrices: function() {
            return comboPrices();
        }
    };
});
