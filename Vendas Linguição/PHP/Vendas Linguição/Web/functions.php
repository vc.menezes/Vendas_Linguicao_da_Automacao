<?php
    require_once('mysqli.php');

    class Functions
    {
        private $MySQLi;

        public function __construct()
        {
            $this->MySQLi = new Connect();
        }

        public function ChecarExistencia($cpf)
        {
            $sql = "SELECT nome from Vendedores WHERE cpf = $cpf";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);
            $no_of_rows = $result->num_rows;

            if ($no_of_rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function CadastraVendedor($nome, $cpf, $ddd, $cel)
        {
            $sql = "INSERT INTO Vendedores (nome, cpf, ddd, celular) VALUES ('$nome', '$cpf', '$ddd', '$cel')";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);

            if ($result)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function ListarVendedores()
        {
            $sql = "SELECT id_vendedor, nome, ddd, celular from Vendedores ORDER BY nome";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);

            echo "<table>
                    <tr>
                        <th>Nome</th>
                        <th>DDD</th>
                        <th>Celular</th>
                        <th></th>
                    </tr>";
            while ($row = $result->fetch_assoc())
            {
                echo "<tr id='".$row['id_vendedor']."'>";
                echo "<td>" . $row['nome'] . "</td>";
                echo "<td>" . $row['ddd'] . "</td>";
                echo "<td>" . $row['celular'] . "</td>";
                echo "<td><img src='../Imagens/delete.png' class='btnDelete'/></td></tr>";
            }
            echo "</table>";
        }

        public function DeletarVendedor($id)
        {
            $sql = "DELETE from Vendedores WHERE id_vendedor = $id";
            $result = $this->MySQLi->query($sql) OR trigger_error($this->MySQLi->error, E_USER_ERROR);

            if ($result)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }