<?php
    header('Access-Control-Allow-Origin: *');
    $data = json_decode(file_get_contents('php://input'), true);
    $tag = $data['tag'];

    if ($tag != '')
    {
        // include db handler
        require_once 'functions.php';
        $db = new Functions();

        // response Array
        $response = array("tag" => $tag, "success" => 0, "error" => 0);

        if($tag == 'listar_vendedores'){
            $vendedores = $db->ListarVendedores();
            // echoing JSON response
            echo json_encode($vendedores);
        }

        if ($tag == 'atualizar_localizacao')
        {
            $lat = $data['latitude'];
            $lng = $data['longitude'];
            $cpf = $data['cpf'];
            $vendedor = $db->UpdateVendedorLocation($cpf, $lat, $lng);

            if ($vendedor != false)
            {
                // user found
                // echo json with success = 1
                $response["success"] = 1;

                echo json_encode($response);
            }
            else
            {
                // user not found
                // echo json with error = 1
                $response["error"] = 1;

                echo json_encode($response);
            }
        }
    }
    else
    {
        echo "Access Denied";
        echo json_encode($data);
    }




