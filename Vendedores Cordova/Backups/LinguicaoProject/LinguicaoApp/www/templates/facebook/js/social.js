/**
 * Created by bonet on 11/5/14.
 */
var socialControllers  = angular.module('social.controllers',[])

socialControllers.controller('InstagramController', function($scope, $http, $window, $ionicModal) {

    var clientID = "bfe40a215d394789b20c5301b5207bb0";
    var count = 5;
    var maxId = null;

    $scope.loadedImages = [];

    $scope.has_more = true;
    var getUrl = function(){
        var url = "https://api.instagram.com/v1/tags/24linguicao/media/recent?";

        url += "client_id="+clientID;
        url += "&count=5";

        if(maxId)
            url += "&max_tag_id="+maxId;

        url += "&callback=JSON_CALLBACK";

        console.log(url);

        return url;
    }
    $scope.loadMore = function() {
        $http.jsonp(getUrl())
            .success(function(data){
                maxId = data.pagination.next_max_tag_id;
                console.log(maxId)
                if (!maxId)
                    $scope.has_more = false;

                console.log(data);
                $scope.loadedImages = $scope.loadedImages.concat(data.data);
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
            })
            .error(function(response){
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.$broadcast('scroll.refreshComplete');
            });
    };

    $ionicModal.fromTemplateUrl('templates/facebook/html/modal-instagram.html', {
        animation: 'slide-in-up',
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.doRefresh = function() {
        $window.location.reload(true);
    }

    $scope.mostra_detail_instagram = function(insta) {
        $scope.insta = insta;
        $scope.modal.show()
    }

    $scope.fechar_modal = function() {
        $scope.modal.hide();
    }
});

