angular.module("devs.controllers",[])
	.controller('DevsController', function($scope){
		function shuffle(o){ //v1.0
    		for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    		return o;
		};


		devs = [{"name":"Eduardo Bonet", 
				 "pic":"img/devs/bonet.jpg",
				 "role": "Desenvolvedor",
				 "linkedin":""},
				 {"name":"Pedro Peralta",
				 "pic":"img/devs/peralta.jpg",
				 "role": "Desenvolvedor",
				 "linkedin":""},
				 {"name":"Charley Felipe",
				 "pic":"img/devs/charley.jpg",
				 "role": "Designer",
				 "linkedin":"",
				 "portif":""}]

		$scope.devs = shuffle(devs);

		
	});
