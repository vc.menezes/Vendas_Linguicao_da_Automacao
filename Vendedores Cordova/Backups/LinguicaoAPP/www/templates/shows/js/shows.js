var showsControllers = angular.module('shows.controllers', []);

/*--------------------------------Lógica para a tela de atrações da festa---------------------------------*/

showsControllers.controller('ShowsController', function($scope, ShowService) {

    var getShowsPacha = function() {
        ShowService.getShowsPacha().then(function (showsPacha) {
            $scope.showsPacha = showsPacha;
        });
    };

    var getShowsStage = function() {
        ShowService.getShowsStage().then(function (showsStage) {
            $scope.showsStage = showsStage;
        });
    };

    getShowsStage();
    getShowsPacha();
});
